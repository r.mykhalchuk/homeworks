let car = {
    manufacturer: "BMW",
    model: "M5",
    year: 2017,
    engine: {
        'litersType': "4.4 liter BMW M Performance TwinPower Turbo V - 8",
        'displacement': 4395,
        'horsepower': 600,
        'compressionRatio': 10.0,
        'torque(lb-ft @ rpm)': 553
    },
    transmission: {
        'type': "8 - speed M STEPTRONIC Automatic transmission with Drivelogic and steering wheel - mounted paddle shifters and Launch Control",
        'automaticGearRatio I / II / III': "5.00 / 3.20 / 2.14"
    },
    performance: {
        'acceleration 0 - 60 mph(sec)': 3.2,
        'topSpeed(mph)': 156
    },
    extteriorDimensions: {
        'length(in)': 195.5,
        'width(in)': 74.9,
        'height(in)': 58.0
    },
    'price(usd)': 110000
}



function toClone(obj) {
    let copiedObj = {};
    for (let key in obj) {
        //console.log(typeof (obj[key]));
        if (typeof (obj[key]) == 'object') {
            copiedObj[key] = toClone(obj[key]);
        } else {
            copiedObj[key] = obj[key];
        }
    }
    return copiedObj;
}

console.log(toClone(car));
